+++
title = 'Lokal verwurzelt, weltweit vernetzt – Lerne dezentrale soziale Netzwerke kennen '
slug = 'lokal-verwurzelt-weltweit-vernetzt'
date = "2022-09-07"
description = 'Abend über das Fediverse'
tags = ["Vortrag","Austausch","Workshop","Bildung","Fediverse"]
+++

## Lokal verwurzelt, weltweit vernetzt – Lerne dezentrale soziale Netzwerke kennen

* Montag, 19. September 2022, 19-21 Uhr
* Haus des Engagements, Freiburg
* Anmeldung erwünscht
* Zahle, was Du willst

Soziale Medien "in gut" - (wie) kann das gehen?</br>
Müssen wir nicht dort sein, "wo alle sind"?</br>
Was verlieren wir, wenn wir den kommerziellen Netzwerken weniger Aufmerksamkeit und Zeit schenken oder ihnen gar ganz den Rücken kehren - und was gewinnen wir?

freiburg.social und die <a href="https://www.stadtwandler.org/de" target="blank">StadtWandler</a> laden ein zu einem Abend über das Fediversum.

### Soziale Medien im Fediversum

Soziale Medien im Fediversum bilden Netzwerke über ihre eigenen Grenzen hinaus. Mikroblogger auf Mastodon folgen Freundinnen auf Friendica, Photofeeds auf Pixelfed oder Konten der Videoplattform PeerTube. Dabei werden Beiträge geteilt, gegenseitig kommentiert und Nachrichten verschickt. Der Wert eines Mediums im Fediversum liegt nicht in der Anzahl seiner Nutzenden sondern in seiner Anschlussfähigkeit. Was bedeutet das für die Qualität des Austauschs? Für die Geschäftsmodelle der beteiligten Akteure? Für die dynamische Weiterentwicklung der eingesetzten Software?

Das Fediversum besteht aus einer beliebig großen Anzahl kleiner Plattformen. Nutzende desselben Mediums wählen aus einer Vielzahl von Softwareinstanzen wo sie ihr Konto einrichten und wem sie ihre Nutzerdaten anvertrauen wollen. Auch im Nachhinein können sie mit ihrem Konto von einer Instanz zu einer anderen umziehen. Viele Nutzende kennen die Menschen persönlich, die den von ihnen genutzten Dienst bereit stellen. Betreiber können Vereine, Organisationen und kommerzielle Anbieter sein oder jede andere Fokusgruppe, die Menschen ähnlicher Interessen zusammen bringt. Auf welche Instanzen hat die Welt bisher vergeblich gewartet? Was muss man mitbringen, um eine Instanz zu gründen? Welche Pflichten und Verantwortungen sind damit verbunden?

Die Veranstaltung im Haus des Engagements bietet Raum, diese Fragen zu diskutieren. Außerdem gibt es Hintergrundinformationen, Impulse und die Möglichkeit, gleich vor Ort ein Konto anzulegen. Angesprochen sind Freiburger Initiativen, Vereine und andere Engagierte, die nach neuen Kommunikationsstrukturen oder Alternativen zu herkömmlichen sozialen Medien suchen, mit dem Gedanken spielen, eine eigene Plattform einzurichten oder einfach nur Interesse am Thema haben.

### freiburg.social

freiburg.social ist ein 2021 eingetragener Verein mit dem Ziel, die Schaffung vertrauenswürdiger, datensparsamer und sicherer digitaler Dienste auf Basis freier Software zu fördern. Selbst betreibt freiburg.social derzeit eine Instanz des verteilten Mikroblogging-Dienstes Mastodon und einen Messenger-Dienst auf Grundlage des Matrix Kommunikationsprotokolls.

### StadtWandler

<a href="https://www.stadtwandler.org/de" target="blank">StadtWandler</a> macht Orte, Aktionen und Ideen für den sozial-ökologischen Wandel sichtbar. Du erfährst, was geschieht in Puncto Nachhaltigkeit in Initiativen, Firmen und Stadtverwaltung. StadtWandler zeigt, was jede:r Einzelne tun kann, um das Freiburg von morgen mitzugestalten und was es bringt. Abonniere die wöchentliche <a href="https://www.stadtwandler.org/aktionspost" target="blank">StadtWandler-Aktionspost</a>

### Anmeldung
Da die Plätze für den Workshop begrenzt sind, bitten wir Euch um kurze <a href="https://www.stadtwandler.org/de/form/lokal-verwurzelt-weltweit-vernet" target="blank">Anmeldung</a>.

### Zahle, was Du willst

Es hat Dir gefallen? Dann freuen wir uns über einen Betrag Deines Ermessens, den wir auf freiburg.social und die StadtWandler aufteilen. Es hat Dir nicht gefallen? Dann zahlst Du auch nichts.</br>
Auf Wikipedia kannst Du noch mehr über das <a href="https://de.wikipedia.org/wiki/Pay_what_you_want" target="blank">Pay-what-you-want-Prinzip</a> erfahren.

### Vorschau
Am Montag, den 10. Oktober 2022, werden wir diese Veranstaltungsreihe mit einem Abend über Matrix, einem dezentralen Messenger aus Freiburg, weiterführen.
