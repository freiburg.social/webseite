---
Title: "Verein"
description: "Wer sind wir überhaupt und wenn ja, wie viele?"
# date: "2021-06-11T00:00:00"
image: 'images/logo.png'
# tags: ["tag1","tag2"]
---

{{< button-group class="actions" >}}
    {{< button title="Über uns" link= "/wir/" class="primary" >}}
    {{< button title="Mitmachen" link= "/mitmachen/" class="primary" >}}
    {{< button title="Satzung" link= "/satzung/" class="primary" >}}
    {{< button title="Finanzierung" link= "/finanzierung/" class="primary" >}}
{{< /button-group >}}
