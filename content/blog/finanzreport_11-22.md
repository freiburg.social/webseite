+++
title = 'Finanzreport November 2022'
slug = 'finanzreport-nov-22'
date = "2022-12-02"
description = 'Transparenzbericht über die Finanzen des Vereins im November 2022'
tags = ["Verein","Finanzen","Finanzreport"]
+++

In diesem Transparenzbericht über unsere Finanzen wollen wir euch über die finanzielle Situation des Vereins im November 2022 informieren. Wir werden diese Berichte nun hoffentlich monatlich herausbringen. Einen Überblick könnt ihr euch auch immer unter [Finanzierung](/finanzierung) verschaffen.

## Spenden

Im November freuten wir uns über **15** Einzelspenden über eine Gesamtsumme von **335,90 €**. Vielen Dank!


## Laufende Kosten

* 11,98 € / Monat für den Mastodon Server. Auf diesem Server laufen auch noch diese Website und unsere Vereinsinterne Nextcloud
* 11,98 € / Monat für den Matrix Server.
* 2,90 € / Monat für die Domain freiburg.social.

Wie ihr seht, können wir derzeit unsere Kosten sehr gut decken. Vermutlich kommen in Kürze weitere Kosten für erhöhte Serverleitung oder Storage hinzu.