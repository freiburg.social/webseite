+++
title = 'Online-Workshop: Gewalt gegen Frauen im Digitalen – Was möglich ist und was Ihr tun könnt'
slug = 'Online-Workshop-Gewalt-gegen-Frauen-im-Digitalen–--Was-möglich-ist-und-was-Ihr-tun-könnt'
date = "2022-11-22"
description = 'Online-Workshop am 10. Januar 2023'
tags = ["Vortrag","Austausch","Workshop","Bildung"]
+++

## Verlegt von Freitag, den 25. November 2022

* Dienstag, 10. Januar 2023, 17-20 Uhr
* Online via Senfcall (BigBlueButton)
* Anmeldung erforderlich
* Zahle, was Du willst


## Gewalt gegen Frauen im Digitalen – Was möglich ist und was Ihr tun könnt
Gewalt findet auch im Digitalen statt, ob im Netz oder direkt durch Überwachung unserer eigenen Geräte. Technisch sind kaum Grenzen gesetzt und oft ist es schwer, zu sehen, wenn jemand uns ins Visier genommen hat. Was alles möglich ist und was Ihr tun könnt, um einer Freundin oder auch Euch selbst zu helfen, erklärt Klaudia Zotzmann-Koch am Dienstag, den 10. Januar 2023 von 17 bis 20 Uhr.

## Referentin
[Klaudia Zotzmann-Koch](https://www.zotzmann-koch.com/) wohnt in Wien und ist Autorin, Podcasterin und Datenschutzexpertin. Neben Krimis, historischen Romanen und SciFi schreibt sie Sachbücher über kreative Themen und Datenschutz. Außerdem hält sie Vorträge und Workshops zu „digitaler Selbstverteidigung“, Medienkompetenz, Technikfolgen, Social Media, Datenschutz, kreatives Schreiben und Podcasting.

## Anmeldung
Wir bitten um Anmeldung an wir@freiburg.social. Die Zugangsdaten werden rechtzeitig versandt.
Wir möchten Euch einladen, mit der Anmeldung konkrete Themen einzureichen, die Euch interessieren und auch Fragen, die im Workshop dann behandelt werden.

## Zahle, was Du willst
Es hat Dir gefallen? Dann freuen wir uns über einen Betrag Deines Ermessens, den wir vollständig an die Referentin weitergegeben. Es hat Dir nicht gefallen? Dann zahlst Du auch nichts.
Auf Wikipedia kannst Du noch mehr über das [Pay-what-you-want-Prinzip](https://de.wikipedia.org/wiki/Pay_what_you_want) erfahren.

## Technische Hinweise
Für den Online-Workshop verwenden wir [BigBlueButton](https://bigbluebutton.org/), ein datensparsames und zuverlässiges Open Source Videokonferenzsystem, das uns von [Senfcall](https://senfcall.de/) zur Verfügung gestellt wird. Für die Teilnahme ist keine Installation notwendig, da BigBlueButton im Browser arbeitet.

## Aufzeichnung
Die Veranstaltung wird nicht aufgezeichnet.
