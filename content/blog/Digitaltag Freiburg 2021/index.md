+++
title = 'Digitaltag Freiburg 2021 - Wir sind dabei!'
slug = 'digitaltag-freiburg-2021'
date = "2021-06-12"
description = 'Mit drei Vorträgen werden wir auf dem diesjährigen Digitaltag Freiburg uns und unsere Dienste vorstellen.'
disqus_identifier = '9'
tags = ["termine","verein","mastodon","matrix"]
+++

Beim [Digitaltag Freiburg](https://www.freiburg.digital) am Freitag, den 18. Juni 2021 dürfen wir natürlich nicht fehlen!


### 10-10:30 Uhr: **freiburg.social - Wir stellen uns vor** - Ein Vortrag von Imke

Mit der Bereitstellung eines sozialen Netzwerks (Mastodon) und eines Messengers (Matrix) möchten wir die Nutzung und Entwicklung freier, datensparsamer, sicherer und föderierter digitaler Kommunikation und Vernetzung fördern und unterstützen. Wir möchten unseren kürzlich gegründeten Verein vorstellen und uns weiter vernetzen.

{{< button-group class="actions fit" >}}
    {{< button title="Hier geht's zur Veranstaltung." link= "https://www.freiburg.digital/veranstaltungen/freiburg.social-wir-stellen-uns-vor/" class="primary" >}}
{{< /button-group >}}


### 10:30-11 Uhr: **freiburg.social: Matrix statt WhatsApp, ein verschlüsselter, freier Messenger.** - Ein Vortrag von Ralf

Wir erklären kurz, was unser Messenger kann und was ihn so besonders macht.
Inhalte: Was ist Freie Software? Was bedeutet "Föderierung"? Welches Problem löst Matrix für mich? Wenn ich das Problem auch habe, wie komme ich jetzt zu einem Account?

{{< button-group class="actions fit" >}}
    {{< button title="Hier geht's zur Veranstaltung." link= "https://www.freiburg.digital/veranstaltungen/freiburg.social-matrix-statt-whatsapp-ein-verschlusselter-freier-messenger/" class="primary" >}}
{{< /button-group >}}


### 11-11:30 Uhr: **freiburg.social: Föderierte, dezentrale soziale Netzwerke gegen Macht- und Datenmonopole** - Ein Vortrag von Niklas

Wir schauen uns die grundlegenden Probleme der großen, zentralen sozialen Netzwerke wie Twitter und Facebook an und stellen als Alternative das Konzept von föderierten Netzwerken vor und wie diese die Probleme lösen.
Hört sich kompliziert an? Ich verspreche, wenn ihr schon einmal eine Email verschickt habt, werdet ihr das Konzept verstehen.
Wenn ihr danach Lust bekommt euch selbst mal im föderierten Universum - dem Fediverse - herumzutreiben, zeige ich euch, wie ihr euch einen Account anlegen könnt.

{{< button-group class="actions fit" >}}
    {{< button title="Hier geht's zur Veranstaltung." link= "https://www.freiburg.digital/veranstaltungen/freiburg.social-foderierte-dezentrale-soziale-netzwerke-gegen-macht-und-datenmonopole/" class="primary" >}}
{{< /button-group >}}
