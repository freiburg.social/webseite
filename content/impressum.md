---
Title: "Impressum"
description: "Wo steht die Beschreibung?"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
size: "medium"
---

**freiburg.social e. V.**

Der Vorstand: Ralf Tauscher, Felix Engel, Martin Folkers

**freiburg.social e. V.**  
im [Haus des Engagements](https://haus-des-engagements.de/)  
Rehlingstraße 9 (Innenhof)  
79100 Freiburg im Breisgau

[wir@freiburg.social](MAILTO:wir@freiburg.social)

Vereinsregisternummer: VR 703251  
Registergericht am Amtsgericht Freiburg

*Diese Angaben gelten ebenfalls für die von uns betriebenen Dienste [Mastodon](https://freiburg.social), Matrix (matrix.freiburg.social) und den zugehörigen [Element-Messenger](https://element.freiburg.social).*

**Logo**  
[Imke Senst](https://agd.de/netzwerk/designer/10769/) (Lizenz: CC BY-SA)  
**UFO**  
Karin Seyfarth, Olav Seyfarth
