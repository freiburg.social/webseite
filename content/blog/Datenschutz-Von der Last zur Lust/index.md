+++
title = 'Datenschutz: Von der Last zur Lust'
slug = 'datenschutz-von-der-last-zur-lust'
date = "2021-06-08"
description = 'Unser Datenschutzbeauftragter Hendrik hat als Referent bei der Deutschen Stiftung für Engagement und Ehrenamt eine Seminarreihe zum Thema "Datenschutz: Von der Last zur Lust" gehalten. Hier gibt es die Links zu den Aufzeichnungen!'
disqus_identifier = '9'
tags = ["Datenschutz","Video"]
+++

Unser Datenschutzbeauftragter Hendrik hat als Referent bei der [Deutschen Stiftung für Engagement und Ehrenamt](https://www.deutsche-stiftung-engagement-und-ehrenamt.de) eine Seminarreihe zum Thema **Datenschutz: Von der Last zur Lust** gehalten. Na, wenn das nicht schon sexy klingt!? Und das Beste: Alle vier Teile sind als Aufzeichnung nachsehbar!

* [Teil 1: Besitz belastet? Vereinsdaten nutzen und schützen](https://www.deutsche-stiftung-engagement-und-ehrenamt.de/dseeerklaert/datenschutz-von-der-last-zur-lust/#toggle-id-1)
* [Teil 2: Verein(t) zu Hause: virtuelle Vereinstreffen und Online-Mitgliederversammlung](https://www.deutsche-stiftung-engagement-und-ehrenamt.de/dseeerklaert/datenschutz-von-der-last-zur-lust/#toggle-id-2)
* [Teil 3: Verklickt und zugenäht? Datenschutz und Öffentlichkeitsarbeit](https://www.deutsche-stiftung-engagement-und-ehrenamt.de/dseeerklaert/datenschutz-von-der-last-zur-lust/#toggle-id-3)
* [Teil 4: Datenschutz - wer macht's? Strategien zur Umsetzung und das Amt des Datenschutzbeauftragten](https://www.deutsche-stiftung-engagement-und-ehrenamt.de/dseeerklaert/datenschutz-von-der-last-zur-lust/#toggle-id-4)
