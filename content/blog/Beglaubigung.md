+++
title = 'Digitaltag Freiburg - Wir waren dabei...'
slug = 'online'
image = 'images/beglaubigung.jpg'
date = "2021-06-18"
description = '... und unsere Unterschriften sind beglaubigt!'
disableComments = true
+++

Puh, war das eine Woche! Wir haben mit vereinten Kräften und einigen Nachtschichten auf den heutigen Tag hingearbeitet. Heute fand der bundesweite [Digitaltag](https://digitaltag.eu/) statt, der in diesem Jahr in Freiburg als [Digitaltag Freiburg](https://www.freiburg.digital/) besonders groß angekündigt wurde. Natürlich durften wir als freiburg.social nicht fehlen! Allerdings ... Anfang der Woche war die Webseite noch nicht fertig programmiert, wir konnten noch nicht zuverlässig E-Mails verschicken (was die Öffnung unserer Mastodon-Instanz ohne Einladung verhinderte), unsere [Matrix-Instanz](https://element.freiburg.social/#/welcome) sah noch so gar nicht nach uns aus und überhaupt - es gab noch einiges zu tun, um so richtig offiziell mit allem an die Öffentlichkeit zu gehen.

Das schreckte uns natürlich nicht ab, sondern spornte uns an! Alle schafften und taten, was sie konnten, damit der Startschuss fallen konnte. Und das tat er heute beim [Digitaltag Freiburg](https://www.freiburg.digital/). Wir waren mit dreieinhalb Beiträgen vertreten:

* [10 Uhr - freiburg.social: Wir stellen uns vor](https://www.freiburg.digital/veranstaltungen/freiburg.social-wir-stellen-uns-vor/)
* [10:30 Uhr - freiburg.social: Matrix statt WhatsApp, ein verschlüsselter, freier Messenger](https://www.freiburg.digital/veranstaltungen/freiburg.social-matrix-statt-whatsapp-ein-verschlusselter-freier-messenger/)
* [11 Uhr - freiburg.social: Föderierte, dezentrale soziale Netzwerke gegen Macht- und Datenmonopole](https://www.freiburg.digital/veranstaltungen/freiburg.social-foderierte-dezentrale-soziale-netzwerke-gegen-macht-und-datenmonopole/)
* [15 Uhr - Datenschutz im Ehrenamt: Die Grundlagen](https://digitaltag.eu/datenschutz-im-ehrenamt-die-grundlagen), zählt nur halb, da zwar von unserem Vereinsmitglied Hendrik gehalten, dies aber im Rahmen seiner Tätigkeit bei der [Stiftung Datenschutz](https://stiftungdatenschutz.org/startseite)

Es war für uns eine spannende Erfahrung, unseren so jungen Verein und unsere Angebote fremden Menschen vorzustellen. Leider hat bei unserer Vorstellung um 10 Uhr aus technischen Gründen die Aufnahme nicht geklappt, aber die Vorträge über Matrix und Mastodon sollten demnächst online nachschaubar sein. Wir werden darüber informieren!

{{< image-text src="/images/geschafft.jpg" class= "left" >}}
Unsere drei Vorstandsmitglieder haben heute jedoch nicht nur Vorträge gehalten, heute war auch der Tag gekommen, an dem wir endlich unsere Unterschriften auf dem Antrag zur Eintragung unseres Vereins ins Vereinsregister beglaubigen lassen sollten. Und das taten wir. Um 14 Uhr betraten wir das Notariat, um 14:30 Uhr war es schon geschafft. Anschließend durfte alles direkt in den Briefkasten des Registergerichts rutschen. Und nun harren wir der Dinge.

Vor unserer Gründungsversammlung, die wir am 26. März online via <a href="https://senfcall.de/" target="blank">Senfcall</a> abgehalten hatten, recherchierten wir lange, ob eine Gründungsversammlung online stattfinden darf. Wir hörten viele Geschichten und lasen Zeitungsartikel, in denen davon berichtet wurde, dass Registergerichte virtuelle Gründungsversammlungen *nicht* anerkannt hätten. Ein letztendlicher Anruf beim Freiburger Registergericht machte uns schließlich Mut, dass es gelingen sollte. Ob die unseren Antrag bearbeitende Mitarbeiterin jedoch der gleichen Meinung sein wird, werden wir sehen. Wir gehen vom Besten aus.
{{< /image-text >}}

Ach ja, unsere Webseite ist mittlerweile online. Unsere [Matrix-Instanz](https://element.freiburg.social/#/welcome) sieht jetzt nach uns und Freiburg aus. Unsere [Mastodon-Instanz](https://freiburg.social) ist nun für alle offen. Und wir sind alle ziemlich müde, aber sehr zufrieden.
