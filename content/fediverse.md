+++
# title = 'Mastodon'
slug = 'fediverse'
image = 'images/mastodon_logo_ueberschrift.svg'
# date = "2021-06-08"
description = 'Auf unserer Mastodon-Instanz für Freiburg und Umland bieten wir Euch ein föderiertes soziales Netzwerk an.'
tags = ["dienste","mastodon"]
+++

Auf unserer Mastodon-Instanz für Freiburg und Umland bieten wir Euch ein föderiertes soziales Netzwerk an. Was genau es mit Mastodon auf sich hat und wie es überhaupt funktioniert, erklärt beispielsweise <a href="https://mobilsicher.de/" target="_blank">Mobilsicher</a> in dem Artikel <a href="https://mobilsicher.de/ratgeber/mastodon-das-bessere-twitter" target="_blank">Mastodon – das bessere Twitter?</a>

Wer lieber hören mag, kann sich ein halbes Stündchen mit dieser Podcastfolge von <a href="https://besser.demkontinuum.de/" target="_blank">Besser - Der Podcast</a> beschäftigen: <a href="https://besser.demkontinuum.de/2018/12/mastodon-s01e06/" target="_blank">Mastodon #S01E06</a>. Im Rahmen einer <a href="https://besser.demkontinuum.de/2021/12/das-fediverse-als-chance/" target="_blank">ganzen Reihe von Fediverse-Episoden</a> erklärt Micha als „Experte“ Lorenz dem „Neuling“ das Fediverse und die einzelnen Plattformen. Erwartet also keinen nerdigen Expertentalk!

Auch wenn es auf Mastodon noch nicht so viele Accounts gibt wie auf den großen, kommerziellen Plattformen, ist die Kommunikationsqualität eine ganz andere. <a href="https://freiburg.social/" target="_blank">Probiert es aus!</a> 🎉

Damit Ihr seht, dass sich dort doch schon einige interessante Institutionen und Menschen tummeln, hier eine kleine Liste mit Accounts, denen es sich zu folgen lohnen könnte:
- freiburg.social 🤩 : <a href="https://freiburg.social/@wir" target="_blank">@wir@freiburg.social</a>
- Stadt Freiburg: <a href="https://bawü.social/@freiburg" target="_blank">@freiburg@bawü.social</a>
- StadtWandler Freiburg: <a href="https://freiburg.social/@StadtWandler" target="_blank">@StadtWandler@freiburg.social</a>
- Computertruhe e. V.: <a href="https://mastodon.social/@computertruhe" target="_blank">@computertruhe@mastodon.social</a>
- Chaos Computer Club Freiburg: <a href="https://chaos.social/@cccfr" target="_blank">@cccfr@chaos.social</a>
- Haus des Engagements: <a href="https://freiburg.social/@hausdesengagements" target="_blank">@hausdesengagements@freiburg.social</a>
- Treffpunkt Freiburg: <a href="https://freiburg.social/@treffpunkt" target="_blank">@treffpunkt@freiburg.social</a>
- Radio Dreyeckland: <a href="https://todon.nl/@RDL" target="_blank">@RDL@todon.nl</a>
- Amnesty International Freiburg: <a href="https://todon.nl/@Amnesty_International_Freiburg" target="_blank">@Amnesty\_International\_Freiburg@todon.nl</a>
- Buchhandlung Fundevogel: <a href="https://freiburg.social/@fundevogel" target="_blank">@fundevogel@freiburg.social</a>
- WEtell.Mobilfunk: <a href="https://chaos.social/@wetell" target="_blank">@wetell@chaos.social</a>
- Demo-Ticker Freiburg: <a href="https://freiburg.social/@demo" target="_blank">@demo@freiburg.social</a>
- Städtische Museen Freiburg: <a href="https://bawü.social/@StaedtischeMuseenFreiburg" target="_blank">@StaedtischeMuseenFreiburg@bawü.social</a>
- Universität Freiburg: <a href="https://bawü.social/@unifreiburg" target="_blank">@unifreiburg@bawü.social</a>
- Galaxy-Team der Uni Freiburg: <a href="https://bawü.social/@galaxyfreiburg" target="_blank">@galaxyfreiburg@bawü.social</a>
- Regierungspräsidium Freiburg: <a href="https://bawü.social/@RPFreiburg" target="_blank">@RPFreiburg@bawü.social</a>
- Landesbeauftragter für den Datenschutz und die Informationsfreiheit Baden-Württemberg: <a href="https://bawü.social/@lfdi" target="_blank">@lfdi@bawü.social</a> Vielen Dank übrigens an <a href="https://www.baden-wuerttemberg.datenschutz.de/" target="_blank">Dr. Stefan Brink</a> für die Initiative, die <a href="https://bawü.social/explore" target="_blank">bawü.social Instanz</a> für öffentliche Stellen in Baden-Württemberg einzurichten! 🍾
- Bundesbeauftragte für den Datenschutz und die Informationsfreiheit: <a href="https://social.bund.de/@bfdi" target="_blank">@bfdi@social.bund.de</a>
- Landesregierung Baden-Württemberg: <a href="https://mastodon.social/@RegierungBW" target="_blank">@RegierungBW@mastodon.social</a>
- Institut für Digitale Ethik: <a href="https://bawü.social/@DigitaleEthik" target="_blank">@DigitaleEthik@bawü.social</a>
- Landeszentrale für politische Bildung Baden-Württemberg: <a href="https://bawü.social/@lpb" target="_blank">@lpb@bawü.social</a>
- Umweltministerium Baden-Württemberg: <a href="https://bawü.social/@Umweltministerium" target="_blank">@Umweltministerium@bawü.social</a>
- Klimaliste Baden-Württemberg: <a href="https://climatejustice.social/@KlimalisteBW" target="_blank">@KlimalisteBW@climatejustice.social</a>
- Bundesamt für Sicherheit in der Informationstechnik: <a href="https://social.bund.de/@bsi" target="_blank">@bsi@social.bund.de</a>
- Deutsche Bundesstiftung für Datenschutz: <a href="https://social.bund.de/@DS_Stiftung" target="_blank">@DS\_Stiftung@social.bund.de</a>

Dass so viele öffentliche Stellen auf Mastodon vertreten sind, liegt (auch) an einer rechtlichen Bewertung der <a href="https://www.baden-wuerttemberg.datenschutz.de/wp-content/uploads/2020/02/DE_Richtlinie-zur-Nutzung-sozialer-Netzwerke-durch-%C3%B6ff.-Stellen-20200205.pdf" target="_blank">Richtlinie zur Nutzung sozialer Netzwerke durch öffentliche Stellen</a> (pdf). Diese besagt, dass „Bürger [dürfen] nicht gezwungen werden [dürfen], für die Kontaktaufnahme oder für Informationen von einer öffentlichen Stelle Social-Media-Plattformen zu nutzen, die Nutzer beispielsweise für Werbung profilieren.“  Das vollstänige Interview mit dem <a href="https://www.baden-wuerttemberg.datenschutz.de/" target="_blank">Landesdatenschutzbeauftragen für Baden-Württemberg</a>, Dr. Stefan Brink, ist auf der Webseite von <a href="https://netzpolitik.org/" target="_blank">Netzpolitik.org</a> nachzulesen: <a href="https://netzpolitik.org/2021/interview-zu-behoerden-in-sozialen-netzwerken-mastodon-ist-kein-vollstaendiger-aber-doch-ein-guter-twitter-ersatz/" target="_blank">„Mastodon ist kein vollständiger, aber doch ein guter Twitter-Ersatz“</a>

Vielleicht fällt auf, dass nicht alle oben aufgeführten Accounts auf unserer eigenen Instanz liegen. Genau wie bei E-Mails, können auch hier Nutzende verschiedener Angebote miteinander kommunizieren: Gmail spricht ebenso selbstverständlich mit GMX wie freiburg.social mit bawü.social und anderen. Das nennt man **Föderation** und wird <a href="https://wir.freiburg.social/blog/post1/">in diesem Artikel</a> erklärt.

Und jetzt...

{{< button-group class="actions fit" >}}
    {{< button title="... komm mit zu Mastodon..." link= "https://freiburg.social/" class="primary" >}}
{{< /button-group >}}

... schreibe Deinen ersten Tröt mit dem Hashtag <a href="https://freiburg.social/tags/neuhier" target="_blank">#neuhier</a> und sei gespannt, was passiert! 😍

### Informationen zum Datenschutz

Unsere Datenschutzinformation für Mastodon ist über die Adresse <a href="https://freiburg.social/terms" target="_blank">freiburg.social</a> abrufbar.
