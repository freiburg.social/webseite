+++
#title = 'Matrix als Messenger'
slug = 'matrix'
image = 'images/matrix_logo_ueberschrift.svg'
description = 'Unsere Matrix-Instanz steht für alle offen, die Alternativen zu WhatsApp und Co. suchen.'
#date: "2021-06-11T00:00:00"
disqus_identifier = '2'
tags = ["dienste","matrix"]
+++

Unsere [Matrix-Instanz](https://element.freiburg.social/) steht für alle offen, die Alternativen zu WhatsApp und Co. suchen.

- Inhalt
  - [(1) Einleitung](#einleitung)
  - [(2) Weitere Dokumentation](#weitere-dokumentation)
  - [(3) Unser Server](#unser-server)

<!--
  - \(2) Schnellstart-Anleitung (mobile App)
  - \(3) Schnellstart-Anleitung (Browser oder Desktop-App)
  - \(4) Einstellungen und erster Chat
  - \(5) Glossar
  - \(6) Fragen...	
-->

<a id="einleitung"></a>

## Einleitung 

Diese Anleitung ist eine “Schnellstart”-Anleitung anhand
verschiedener “Element”-Versionen für den Matrix-Server von freiburg.social.  

**Ziele**:

- du erfährst etwas über die Idee der Matrix
- installierst einen Client (die App "Element")
- erstellst deinen Account
- startest deinen ersten Chat

<a id="warum-matrix-element"></a>

### Warum Matrix / Element?
Matrix ist ein **dezentraler**, _föderierter_ Kommunikationsstandard. 
Im Prinzip ist es wie ein E-Mail-System, 
jedoch moderner, 
mit viel mehr Möglichkeiten und sicherer:
**Ende-zu-Ende-Verschlüsselung** ist vorausgewählt [1]. 

- **Dezentral**: Jede und jeder kann einen eigenen Server betreiben.
Es gibt also keine zentrale Organisation, 
die alle Server kontrolliert.
- _Föderiert_: Die Server können miteinander kommunizieren.
Menschen müssen also nicht alle auf den selben 
Servern angemeldet sein, 
um miteinander kommunizieren zu können.

Über *51 Mio. Nutzer* haben bereits Matrix-Accounts 
(Stand Anfang 2022). 
Eingesetzt wird Matrix unter anderem von Abteilungen 
der französichen Regierung, 
der Bundeswehr (BW-Messenger), 
vielen Unis, und bald auch von Institutionen 
des deutschen Gesundheitswesens.

Element ist die Referenzimplmentierung und ist unserer Meinung nach für ein breites Spektrum von Nutzern geeignet. Sobald du dich sicher fühlst, probiere gerne andere Matrix-Apps aus (z. B. FluffChat).

<a id="was-kann-matrix-element"></a>

### Was kann Matrix / Element?

Matrix bietet viele Möglichkeiten, die Kommunikation auf einer Plattform zu bündeln und dadurch eine Zusammenarbeit zu optimieren.

- **private verschlüsselte Chats und Gruppen**
- **öffentliche Gruppen** mit mehreren tausend Personen
- **Text, Links, Bilder, Videos, Sprachnachrichten, Umfragen, Standort**
- **Flexibilität** - Verwendung _unabhängig_ von einer Telefonnumer auf x Handys, Tablets und Desktop-Computern.
- **Formatierung** mit Markdown-Syntax
- **verschlüsselte Sprach- und Videokonferenzen**
- **Spaces**, Ordner für Räume, damit ihr Arbeitsgruppen bilden könnt, auf die alle im Space Zugriff haben.
- **Andere Tools und Messenger einbinden** wie z.B. Pads,
Telegram, Signal, Twitter, Blog-Feeds oder E-Mail-Newsletter.

<a id="weitere-dokumentation"></a>

## Weitere Dokumentation

Diese Webseite ist im Aufbau. Weiterer Inhalt wird zeitnah folgen. Derweil verweisen wir gerne auf die Informationen und Hilfeseiten der TU-Dresden: [Matrix an der TU Dresden](https://doc.matrix.tu-dresden.de/)

<a id="unser-server"></a>

## Unser Server

Willst Du unseren Server für Deine Chats nutzen, findest Du hier die wichtigsten Daten für die Nutzung:

- Web-Oberfläche zum chatten (keine Installation erforderlich): https://element.freiburg.social
- Homeserver für die Konfiguration Deiner App: `https://matrix.freiburg.social`
- Deine Chat-Adresse wird wie folgt aussehen: `@benutzername:freiburg.social`

{{< button-group class="actions fit" >}}
    {{< button title="Komm mit zu Matrix" link= "https://element.freiburg.social/" class="primary" >}}
{{< /button-group >}}
