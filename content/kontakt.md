---
Title: "Kontakt"
description: "Wo steht die Beschreibung?"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
size: "medium"
---

Wir sind ganz wunderbar erreichbar über  

- [Mastodon](https://freiburg.social) und unseren 
- [öffentlichen Matrix-Space](https://matrix.to/#/!CCYZRegGeFrnXzSXIz:yatrix.org?via=yatrix.org&via=freiburg.social&via=matrix.org). 
- Über [E-Mails](mailto:wir@freiburg.social) freuen wir uns durchaus auch.
