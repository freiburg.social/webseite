---
Title: "Finanzierung"
description: "Was kosten Eure Dienste und warum nichts?"
# date: "2019-02-06T00:00:00"
# image: 'images/logo.svg'
# tags: ["tag1","tag2"]
---

### Transparenz

Einer der Begriffe, die wir uns auf die Fahnen schreiben, lautet **Transparenz**. Wir möchten transparent und vertrauensvoll sein. Und das nicht mit Green-Washing, sondern mit Daten und Fakten. Wir möchten Euch zeigen, wer die [Menschen](/wir/) hinter freiburg.social e. V. sind. Und wir möchten Euch zeigen, wie wir uns **finanzieren**.


### Was verdienen wir, indem wir Euch unsere Dienste anbieten?

Alle unsere Vereinsmitglieder arbeiten ehrenamtlich und ohne Entgelt. Es ist uns ein persönliches Anliegen, ein Bedürfnis sozusagen, "vertrauenswürdige(n), datensparsame(n) und sichere(n) digitale(n) Dienste(n) auf Basis von freier Software" (Auszug aus unserer Satzung) zu fördern, bereitzustellen und darüber zu informieren. Gerne nehmen wir jedoch Spenden entgegen, um unsere Ausgaben zu decken.

### Einnahmen

Aus verschiedenen Gründen möchten wir auf Mitgliedsbeiträge verzichten. Ebenso möchten wir keine Nutzungsgebühren für unsere Dienste erheben. Wir möchten, dass alle Menschen, unabhängig von ihrer finanziellen Situation, unsere Angebote nutzen können.

### Spenden

Wir freuen uns über Spenden, mit denen wir hauptsächlich unsere Serverkosten decken.
Wenn ihr regelmäßig spenden wollt, denkt daran, dass wir für jede Buchung eine Gebühr
an die Bank abdrücken müssen, das ist besonders bei kleinen Beträgen relevant. Spendet
also z. B. lieber 12 € im Jahr als 1 € im Monat.<br/>
Daueraufträge, aber auch Einmalzahlungen in beliebiger Höhe helfen uns weiter.
PayPal möchten wir nicht anbieten. Weitere Zahlungsweisen müssen wir erst noch prüfen.<br/>
Wir sind übrigens **kein** gemeinnütziger Verein. Spendenbescheinigungen können
wir daher nicht ausstellen. Wir hoffen auf Euer Verständnis.

Die Bankverbindung lautet:<br/>
__freibug.social e.V.
IBAN: DE45 5003 1000 1096 2180 08__

#### 2022

Stand: 31. Dezember 2022

| **Wann?** | **Was?** | **Betrag** |
|:--|:--|:--|
| Dezember | Spenden | 186,- € |
| November | Spenden | 395,90 € |
| Oktober | Spenden | 280,- € |
| September | Spenden | 10,- € |
| August | Spenden | 10,- € |
| Juli | Spenden | 10,- € |
| Juli | Förderung Freiburger Projektefonds | 350,- € |
| Juni | Spenden | 143,- € |
| *2022* | *Gesamtbetrag* | *1.384,90 €*

#### 2021

| **Wann?** | **Was?** | **Betrag** |
|:--|:--|:--|
| August | Förderung Freiburger Projektefonds | 100,- € |
| *2021* | *Gesamtbetrag* | *100,- €*

### Ausgaben

Wir möchten genau darstellen, welche Ausgaben wir für was haben. Solltet Ihr Euch über einzelne Punkte - oder auch alle - wundern, tauschen wir uns gerne mit Euch darüber aus. Wir wählen die Ausgaben nach bestem Wissen und Gewissen und wägen dabei verschiedene Kriterien ab.
Stand: 31. Dezember 2022

#### 2022 - Hosting

| **Wann?** | **Was?** | **An wen?** | **Betrag pro Monat** | **Gesamt 2022** |
|:--|:--|:--| :--| :--|
| Monatlich | Domain *freiburg.social* | Strato | 2,90 € | 17,40 €
| Monatlich, Februar - Oktober| Server für Mastodon, Nextcloud, E-Mail und die Website | netcup | 10,- € | 90,- €
| Monatlich, ab November| Server für Mastodon, Nextcloud, E-Mail und die Website | netcup | 11,98 € | 23,96 €
| Monatlich, ab Dezember | Backup-Speicher | netcup | 5,- € | 5,- € |
| Monatlich, seit Januar 2022 | Server für Matrix | netcup | 11,98 € | 143,76 € |
| *2022* | *Gesamtbetrag* | | | *280,12 €*

#### 2022 - Andere Ausgaben

| **Wann?** | **Was?** | **An wen?** | **Betrag** |
|:--|:--|:--|:--|
| Juni-Dezember | Bankgebühren | Triodos Bank | 3,60 € |
| August | Eintragung Vereinsregister | Landesoberkasse BW | 95,- € |
| Juni | Druck Postkarten | Citydruck Freiburg | 100,- € |
| Mai | Druck Roll Up | Umweltdruckhaus | 122,57 € |
| Mai | Druck Plakate | Copyshop | 8,- €
| März | Druck Flyer | Die Umweltdruckerei | 87,28 € |
| *2022* | *Gesamtbetrag* | | *416,45 €*

#### 2021 - Hosting

| **Wann?** | **Was?** | **An wen?** | **Betrag pro Monat** | **Gesamt 2021** |
|:--|:--|:--| :--| :--|
| Monatlich | Domain *freiburg.social* | Strato | 2,90 € | 17,40 €
| Monatlich, seit Februar 2021 | Server für Mastodon, Nextcloud, E-Mail und die Website | netcup | 10,- € | 110,- €
| Monatlich, seit Januar 2021 | Server für Matrix | netcup | 6,- € | 72,- € |
| *2021* | *Gesamtbetrag* | | *18,90 €* | *199,40 €*

#### 2021 - Andere Ausgaben

| **Wann?** | **Was?** | **An wen?** | **Betrag** |
|:--|:--|:--|:--|
| Dezember | Druck Sticker | DeineStadtKlebt.de | 90,04 € |
| Juni | Vereinsregisteranmeldung | Notar | 23,80 € |
| *2022* | *Gesamtbetrag* | | *113,84 €*

#### 2020 - Hosting

| **Wann?** | **Was?** | **An wen?** | **Betrag pro Monat** | **Gesamt 2020** |
|:--|:--|:--| :--| :--|
| Monatlich, seit September 2020 | Domain *freiburg.social* | Strato | 2,90 € | 11,60 € |
| *2020* | *Gesamtbetrag* | | *11,60 €* | *11,60 €* |
