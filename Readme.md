Server starten:
```
hugo server
```

Webseite in Browser öffnen:
```
http://localhost:1313/ (bind address 127.0.0.1)
```

Neue Seite:
```
hugo new seitenname.md
```

Neuer Inhalt:
```
hugo new posts/my-first-post.md
```

Server beenden:
```
Strg+c
```

Änderungen / Ergänzungen einzeln bestätigen:
```
git add -p
```

Status prüfen, um Dateien einzufügen:
```
git status
```

Dateien einfügen:
```
git add ...
```

Branch erstellen:
```
git switch -c "Name"
```

Commiten:
```
git commit
```

Pushen:
```
git push
```

## Favicon
Erstellt mit https://realfavicongenerator.net
Original svg liegt in static/favicon.svg

## Deploy

Das Setup für das automatische Überwachen eines git branches läuft wie folgt:

Die Files `website_update.service` und `website_update.timer` müssen nach /etc/systemd/system kopiert werden und der timer dann mit `systemctl start website_update.timer` gestartet werden. Dann wird alle 5 Minuten der git branch gepullt und bei Bedarf die website neu gebaut. Der Webserver sollte einfach nur auf das `public`-Verzeichnis verweisen, dann sind die Änderungen automatisch online.

```
  __          _ _                                      _       _
 / _|        (_) |                                    (_)     | |
| |_ _ __ ___ _| |__  _   _ _ __ __ _   ___  ___   ___ _  __ _| |
|  _| '__/ _ \ | '_ \| | | | '__/ _` | / __|/ _ \ / __| |/ _` | |
| | | | |  __/ | |_) | |_| | | | (_| |_\__ \ (_) | (__| | (_| | |
|_| |_|  \___|_|_.__/ \__,_|_|  \__, (_)___/\___/ \___|_|\__,_|_|
                                 __/ |
                                |___/
```
