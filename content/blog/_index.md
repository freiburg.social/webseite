+++
Title = "Blog"
+++

Hier gibt es teils eigene, teils fremde Erklärungen und Anleitungen zu allem, was mit Freier Software, föderierten Netzwerken, Datenschutz, Privatsphäre, Nachhaltigkeit, ... zu tun hat.
