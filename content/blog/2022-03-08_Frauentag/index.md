+++
title = 'Frauen* sicherer unterwegs im Netz'
slug = 'frauen-sicherer-unterwegs-im-netz'
date = "2022-02-18"
description = 'Unser Online-Workshop zum Internationalen Frauentag 2022'
tags = ["Verein","Frauen","Workshop","Bildung","Datenschutz"]
+++

## Unser Online-Workshop zum Internationalen Frauentag 2022

* Dienstag, 8. März 2022, 17-20 Uhr
* Online via Senfcall (BigBlueButton)
* Anmeldung erforderlich
* Zahle, was Du willst

Wir alle nutzen das Internet täglich auf die verschiedensten Arten und Weisen: eMail, Messenger, Shopping, SocialMedia, Streaming … und sind dabei mit der ganzen Welt in Kontakt, denn das Internet macht an keiner Ländergrenze Halt. Auch nicht vor Trends wie Blockchain oder NFT. Und leider bietet es auch zahlreiche Möglichkeiten, Gewalt gegen Frauen und Minderheiten auszuüben, von Mobbing, über Stalking bis hin zu sehr konkreten Drohungen.</br>
Aber wie funktioniert das Internet eigentlich, welche Daten fließen von wo nach wo und an welcher Stelle kann es kritisch werden? Und wie kann man sich schützen vor übergriffigem Verhalten?</br>
In dem Workshop erklärt Klaudia Zotzmann-Koch die Hintergründe und zeigt anschaulich, warum manche Apps, Netzwerke und auch Dienste sinnvoller sind als andere und wie leicht es geht, sie zu nutzen. Es werden konkrete Lösungen und auch Hilfsangebote für von digitaler Gewalt Betroffenen vorgestellt.

Der Workshop ist auf drei Stunden geplant, zwei bis drei kurze Pausen werden wir an geeigneter Stelle machen, während denen ggf. auch Spezialfragen geklärt werden können.

### Referentin

{{< image-text src="/images/Klaudia_Zotzmann-Koch_2021_Copyright_Klaudia_Zotzmann-Koch_03_web.jpg" class= "right" alt="Portraitfoto von Klaudia Zotzmann-Koch" >}}

<a href="https://www.zotzmann-koch.com" target="blank">Klaudia Zotzmann-Koch</a> wohnt in Wien und ist Autorin, Podcasterin und Datenschutzexpertin. Neben Krimis, historischen Romanen und SciFi schreibt sie Sachbücher über kreative Themen und Datenschutz. Außerdem hält sie Vorträge und Workshops zu „digitaler Selbstverteidigung“, Medienkompetenz, Technikfolgen, Social Media, Datenschutz, kreatives Schreiben und Podcasting. Wir freuen uns riesig, zum Internationalen Frauentag eine so engagierte Workshopgeberin gefunden zu haben!

{{< /image-text >}}

### Anmeldung

Wir bitten um Anmeldung an <a href="mailto:wir@freiburg.social?subject=Anmeldung%20Frauen%20sicherer%20unterwegs%20im%20Netz&amp;body=Hallo%20freiburg-social,%0D%0A%0D%0Ahiermit%20melde%20ich%20mich%20zum%20Online-Workshop%20 	&bdquo;Frauen%20sicherer%20unterwegs%20im%20Netz&ldquo;%20am%208.%20März%202022%20an.">wir@freiburg.social</a>. Die Zugangsdaten werden rechtzeitig versandt.</br>
Wir möchten Euch einladen, mit der Anmeldung konkrete Themen einzureichen, die Euch interessieren und auch Fragen, die im Workshop dann behandelt werden.

### Zahle, was Du willst

Es hat Dir gefallen? Dann freuen wir uns über einen Betrag Deines Ermessens, den wir vollständig an die Referentin weitergegeben. Es hat Dir nicht gefallen? Dann zahlst Du auch nichts.</br>
Auf Wikipedia kannst Du noch mehr über das <a href="https://de.wikipedia.org/wiki/Pay_what_you_want" target="blank">Pay-what-you-want-Prinzip</a> erfahren.

### Technische Hinweise

Für den Online-Workshop verwenden wir <a href="https://bigbluebutton.org/" target="blank">BigBlueButton</a>, ein datensparsames und zuverlässiges Open Source Videokonferenzsystem, das uns von <a href="https://senfcall.de/" target="blank">Senfcall</a> zur Verfügung gestellt wird. Für die Teilnahme ist keine Installation notwendig, da BigBlueButton im Browser arbeitet.

### Aufzeichnung

Die Veranstaltung wird *nicht* aufgezeichnet.

### Vorschau
Für den 25. November, dem Internationalen Tag zur Beseitigung von Gewalt gegen Frauen, planen wir eine Fortsetzung dieses Workshops, der jedoch auch ohne Teilnahme am 8. März besucht werden kann. An diesem Termin wird der Fokus noch stärker auf digitaler Gewalt gegen Frauen liegen.
