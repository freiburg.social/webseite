+++
title = 'Empfehlungen'
slug = 'empfehlung'
date = "2021-06-06"
description = 'Es gibt richtig viele tolle Initiativen, die wir hier gerne empfehlen möchten.'
disqus_identifier = '8'
tags = ["Empfehlung","Link"]
+++

Es gibt richtig viele tolle Initiativen, die wir hier gerne empfehlen möchten. Und ja, die Liste wird stetig erweitert - und über Tipps für weitere Links freuen wir uns!

* [Mobilsicher](https://mobilsicher.de) - Das Infoportal für sichere Handynutzung
* [Digitalcourage](https://digitalcourage.de/)
