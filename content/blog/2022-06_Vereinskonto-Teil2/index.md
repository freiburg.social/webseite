+++
title = 'Wir haben endlich ein Vereinskonto'
slug = 'vereinskonto_teil2'
date = "2022-06-25"
description = 'The long and winding road'
disableComments = true
tags = ["Verein","Finanzen"]
+++

Letzte Woche war es endlich so weit: Wir konnten uns zum ersten Mal in das Online-Banking unseres Vereinskontos einloggen. Wir hatten ja bereits im Januar [in einem Blogbeitrag](/blog/vereinskonto) über unsere Suche nach der richtigen Bank und unsere Entscheidung für das gGeschäftskonto bei der Triodos Bank berichtet.


## Was ist in der Zwischenzeit geschehen?

Seit der Entscheidung für das Konto ist ungefähr ein halbes Jahr vergangen. Für die unerwartet lange Dauer der Kontoeröffnung gibt es im Grunde zwei Gründe:

Der erste Faktor ist einfach eine Überlastung der Bank. Bereits beim ersten Kontakt wurde uns mitgeteilt, dass der Prozess aufgrund hoher Nachfrage länger als erwartet dauern kann. Anscheinend interessieren sich gerade viele Menschen für nachhaltige Finanzprodukte. Leider hat uns diese Verzögerung wohl mehrfach betroffen. Zunächst nach dem ersten Einreichen unseres Antrags und dann nochmal mindestens einmal, als wir noch fehlende Unterlagen nachreichen mussten.

Eine dieser Nachreichungen war die Steuernummer des Vereins und die war auch gleichzeitig der zweite große Faktor bei der Verzögerung: Je nach Vereinstätigkeit und auch abhängig vom zuständigen Finanzamt, kann es sein, dass für den Verein eine Steuernummer vergeben wird. Diese muss dann auch der Bank mitgeteilt werden, bevor ein Konto eröffnet werden kann.

Also haben wir uns beim zuständigen Finanzamt Freiburg-Stadt gemeldet. Nach ein bisschen hin und her, war dann klar, dass wir den "Fragebogen für die steuerliche Erfassung von Vereinen" ausfüllen müssen. Ein ca. 16-seitiges Dokument in dem Informationen zu den geplanten wirtschaftlichen Aktivitäten des Vereins abgefragt werden.

Auch auf die Bearbeitung dieses Fragebogens mussten wir einige Wochen warten, aber schließlich konnte uns mitgeteilt werden, dass für unseren Verein keine Steuernummer vergeben wird. Der eingeschlagene Weg war aber dennoch der richtige, das die Vergabe - zumindest beim Finanzamt Freiburg-Stadt - in jedem Einzelfall geprüft wird.

Nachdem wir diese Information der Bank mitgeteilt hatten, folgten dann letzte Woche endlich die Zugangsdaten für das Onlinebanking.

## Wie geht's weiter

Wir können nun endlich unsere Finanzen über ein eigenes Konto abwickeln und sind insbesondere in der Lage über SEPA-Überweisungen Spenden zu beziehen.

Die Bankverbindung lautet:

__freibug.social e.V.  
IBAN: DE45 5003 1000 1096 2180 08__

In Zukunft schaffen wir es hoffentlich monatliche Transparenzberichte über unsere [Finanzen](/finanzierung) zu posten.
