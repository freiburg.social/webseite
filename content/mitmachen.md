---
Title: "Mitmachen"
description: "Infos zur Mitgliedschaft"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
size: "medium"
---

Interesse bei uns im Verein mitzumachen?

- Dann schreib uns gerne eine [E-Mail](mailto:wir@freiburg.social)
- Oder auf [Mastodon](/mastodon/) unter [@wir@freiburg.social](https://freiburg.social/@wir)
- Oder per [Matrix](/matrix/) an [@wir:freiburg.social](https://matrix.to/#/@wir:freiburg.social)

Wir freuen uns auf Dich. Komme gerne zu einem unserer nächsten Treffen (jeden zweiten Mittwoch – siehe [Kalender](/termine/). Im Kalender ist auch der Link zu unserem online stattfindenden Treffen zu finden.

**Engagiere dich in einer Arbeitsgruppe**

Du bist auch herzlich eingeladen einfach in einem der Arbeitsgruppen mitzumachen - ohne Vereinsmitgliedschaft.

