---
Title: "Über uns"
description: "Wo steht die Beschreibung?"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
size: "medium"
---

### Wer sind wir überhaupt und wenn ja, wie viele?

{{< image-description src="/images/gemini_200.png" >}}
Wir sind neun an freier Software interessierter Menschen aus Freiburg und Umgebung. Im März 2021 haben wir den Verein freiburg.social e. V. gegründet, der bisher eine Matrix- und eine Mastodon-Instanz anbietet. Wir möchten soziale Netzwerke anbieten, die nicht nur datensparsam, sondern auch datenschutzfreundlich sind. Im sogenannten <a href="https://digitalcourage.de/digitale-selbstverteidigung/fediverse" target="blank">Fediverse</a> fühlen wir uns sicher und zu Hause.
{{< /image-description >}}

<!-- TODO: Format ist noch falsch, der Absatz darf nicht kleiner sein als das Foto (200x200 px)
#### Alex
* Mastodon

#### Bastian
* Mastodon

#### Christopher
* Fördergelder
-->

#### Felix

Alles zentral kann jeder. Digitale Freiheit ist für mich etwas ganz anderes: Den Anbieter, die Software selbst wählen. Immer wieder verrückt: Kaum macht man's richtig, funktioniert's! Gemeinsam gelingt das viel öfter als allein. Das ist mein Antrieb für "die" <a href="https://matrix.org" target="blank">[matrix]</a> und freiburg.social.

#### Hendrik

{{< image-description src="/images/hendrik.jpg" >}}
Ich sehe in freier Software und föderierten Netzen ein großes Potential für datenschutzfreundliche Dienste. Viele der öffentlich angebotenen freien Instanzen sind zwar datensparsam, aber weit davon weg, datenschutzkonform im Sinne der DSGVO zu sein. Bei freiburg.social habe ich den Hut des Datenschutzbeauftragten auf und wirke darauf hin, dass wir hier mit gutem Beispiel voran gehen.
{{< /image-description >}}

#### Imke

{{< image-description src="/images/imke.jpg" >}}
Je mehr ich hier über freie Software und föderierte Netzwerke lerne, desto spannender finde ich diese. Als reine Nutzerin vertrete ich bei uns die Nicht-Profis und setze mich dafür ein, dass das, was wir machen, für alle verständlich wird. Ich bin für Gestaltung und Content auf der Webseite zuständig. Außerdem bin ich im Vereinsvorstand. Noch mehr lernen möchte ich über Datenschutz.
{{< /image-description >}}

#### Niklas

{{< image-description src="/images/niklas.jpg" >}}
Die Idee von dezentralen Netzwerken fasziniert mich, dabei ist sie gar nicht neu, sondern eigentlich die Grundlage des Internets. Seit über 10 Jahren lebe ich in Freiburg und es fehlt mir bislang eine Möglichkeit mich online mit anderen Menschen aus der Region zu vernetzen, ohne dabei in proprietären Systemen eingeschlossen zu sein. Daher habe ich mich für eine Freiburger Mastodon Instanz eingesetzt und dabei aus Versehen den Verein freiburg.social mitbegründet. Derzeit bin ich als Kassenwart im Vorstand.
{{< /image-description >}}

#### Olav

{{< image-description src="/images/olav.jpg" >}}
Ich versuche, Datenschutz und IT-Sicherheit zu leben, probiere aus, was für mich funktioniert. Das Konzept der verteilten Datenhaltung spricht mich an. Ich betreue unsere Nextcloud und möchte mehr über Container und Ansible lernen.
{{< /image-description >}}

<!-- TODO: Format ist noch falsch, der Absatz darf nicht kleiner sein als das Foto (200x200 px)
#### Ralf
{{< image-text src="/images/ralf.png" class= "left" >}}
* Vorstand
* Matrix
{{< /image-text >}}
-->


### Verein

Die ersten von uns lernten sich während der Corona-Pandemie kennen. Na klar, online! Auf [Mastodon](/dienste/mastodon/) und in einer Freiburger [Matrix](/dienste/matrix/)-Gruppe. Am 29. November 2020 haben wir dann unsere **erste Videokonferenz** (na klar, über [Senfcall](https://senfcall.de/) - BigBlueButton) abgehalten. Seitdem treffen wir uns regelmäßig alle 2 Wochen, 21-22 Uhr. Nein, wir überziehen die Stunde nicht. Also, ganz selten mal. Also ... Naja, ab 22 Uhr darf man wieder gehen - wenn es unbedingt sein muss. Nein! Wir bauen keinen sozialen Druck auf! Aber entweder man macht halt mit - oder nicht!

Spaß beiseite ;-)

Seit dem ersten Treffen im November haben vielleicht schon zwanzig Leute mindestens einmal an unseren Treffen teilgenommen. Zwischenzeitlich war beschlossen worden, dass wir einen Verein gründen möchten. Am 29. März 2021 - also genau vier Monate nach dem ersten Treffen (ja, dieses Datum war völlig zufällig das einzige, an dem alle Zeit hatten) - haben wir dann [senf-würzig](https://senfcall.de/) unsere **Gründungsversammlung** abgehalten. Es waren neun Menschen dabei, die sich bis dahin auch zum harten Kern der Aktiven herauskristallisiert hatten. (Es gibt übrigens auch die öffentliche [freiburg.social-Matrix-Gruppe](https://element.freiburg.social/#/room/#freiburg.social:gnubox.org), in der einige dabei sind, die teilweise auch schon bei unseren Treffen dabei waren, die unsere Arbeit aber auch irgendwie begleiten, uns beraten und mitdenken. Herzliche Einladung!)  
Der nächste Meilenstein wird am 18. Juni 2021 vollbracht: die drei gewählten Vereinsvorstehenden werden beim Notar ihre Unterschriften beglaubigen lassen, damit die Eintragung von freiburg.social beim Registergericht beantragt werden kann und wir uns endlich freiburg.social e. V. schimpfen dürfen.


### Mitmachen?

Solltest Du Interesse haben, aktiv in unserem Verein dabei zu sein, kannst Du Dich super gerne bei uns melden! Wir treffen uns derzeit am ersten und dritten Mittwoch eines jeden Monats um 21 Uhr, online. Bei einem solchen Treffen kannst Du uns völlig unverbindlich kennenlernen und schnuppern, ob wir zueinander passen könnten.

{{< button-group class="actions fit" >}}
    {{< button title="Nimm gerne Kontakt mit uns auf!" link= "/kontakt/" class="primary" >}}
    {{< button title="Alle Termine" link= "/termine/" class="primary" >}}
{{< /button-group >}}
