---
Title: "Datenschutzinformation gemäß DSGVO"
description: "Wo steht die Beschreibung?"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
## size: "medium"
---

### Angaben zum Verantwortlichen

**freiburg.social e. V.**  
im Haus des Engagements  
Rehlingstraße 9 (Innenhof)  
79100 Freiburg im Breisgau

[verein@freiburg.social](mailto:verein@freiburg.social)

Weitere Angaben zum Verein und dem aktuellen Vorstand, siehe [Impressum](/impressum/).


### Datenschutzbeauftragter

Der Verein hat Hendrik vom Lehn zum Datenschutzbeauftragten gemäß
Art. 37 DSGVO benannt. Er ist per E-Mail unter 
[datenschutz@freiburg.social](mailto:datenschutz@freiburg.social) oder 
unter der oben angegebenen postalischen Anschrift mit dem Zusatz 
Datenschutzbeauftragter erreichbar.


### Verarbeitung personenbezogener Daten – Zwecke und Rechtsgrundlagen



### Datenverarbeitung auf unserer Vereinswebseite

#### Logfiles

Bei Aufruf unserer Vereinswebseite werden Logeinträge mit IP-Adresse und Uhrzeit 
generiert und bleiben für 10 Tage gespeichert.
Rechtsgrundlage ist unser berechtigtes Interesse gemäß Art. 6. 1 f) DSGVO zum 
Zweck der Anlayse von Fehlern und Sicherheitsvorfällen.

#### Empfänger

Wir betreiben unsere Webseite auf Servern der Firma [Netcup](https://www.netcup.de/).


### Betroffenenrechte

Wenn wir personenbezogene Daten von dir verarbeiten, hast du folgende Rechte:
* ein Recht auf Auskunft über die verarbeiteten Daten und auf Kopie,
* ein Berichtigungsrecht, wenn wir falsche Daten über dich verarbeiten,
* ein Recht auf Löschung, es sei denn, dass noch Ausnahmen greifen, warum wir 
die Daten noch speichern, also zum Beispiel Aufbewahrungspflichten oder 
Verjährungsfristen
* ein Recht auf Einschränkung der Verarbeitung,
* ein jederzeitiges Recht, Einwilligungen in die Datenverarbeitung zu widerrufen,
* ein Widerspruchsrecht gegen eine Verarbeitung im öffentlichen oder bei 
berechtigtem Interesse,
* ein Recht auf Datenübertragbarkeit,
* ein Beschwerderecht bei einer Datenschutz-Aufsichtsbehörde, wenn du der 
Ansicht bist, dass wir deine Daten nicht ordnungsgemäß verarbeiten. 
Für unseren Verein ist der Landesbeauftragte für den Datenschutz und die 
Informationsfreiheit in Baden-Württemberg zuständig. Wenn du dich in einem 
anderen Bundesland oder nicht in Deutschland aufhälst, kannst du dich aber 
auch an die dortige Datenschutzbehörde wenden.


### Datenschutzinformationen zu den von uns betriebenen Diensten

#### Mastodon

Die Datenschutzinformation zu dem von uns betriebenen Mastodon-Server, 
ist unter dessen Adresse [freiburg.social](https://freiburg.social/terms) 
abrufbar.

#### Matrix

Die Datenschutzinformation zu dem von uns betriebenen Matrix-Server, 
ist [hier auf dieser Webseite](/datenschutz-matrix/) abrufbar.


### DS-GVO.clever

Das Grundgerüst dieser Datenschutzinformation haben wir mit Hilfe des [DS-GVO.clever-Tools](https://www.baden-wuerttemberg.datenschutz.de/ds-gvo.clever/) der Baden-Württembergischen Datenschutzaufsichtsbehörde erstellt und an unsere Bedürfnisse angepasst.
