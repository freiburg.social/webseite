+++
title = 'Lerne einen dezentralen Messenger aus Freiburg kennen'
slug = 'lokalverwurzelt-weltweit-vernetzt–lerne-einen-dezentralen-messenger-aus-freiburg-kennen'
date = "2022-09-30"
description = 'Abendveranstaltung "Dezentraler Messenger im Fokus" am 10. Oktober'
tags = ["Vortrag","Austausch","Workshop","Bildung","Messenger","Matrix"]
+++

## Lokal verwurzelt, weltweit vernetzt<br/> Lerne einen dezentralen Messenger aus Freiburg kennen

* **Montag, 10. Oktober 2022, 19-21 Uhr**
* Haus des Engagements, Freiburg
* Anmeldung erwünscht

Willkommen in der Matrix!  
Im Workshop erfährst du, warum es sich lohnt, 
den Freiburger dezentralen Matrix-Messenger zu nutzen statt Whatsapp und Co.

- Du willst WhatsApp loswerden aber deine Kontakte behalten?
- Du bist genervt von vier verschiedenen Messenger-Apps
- Du hast keinen Bock mehr, deine Daten an große Konzerne verkaufen?
- Du willst Fotos deiner Kinder gefahrlos mit der Familie teilen?
- Du machst dir Sorgen, wer mit deinen Kindern chattet?
- Du willst auch ohne Handynummer oder Smartphone einen Messenger benutzen?
- Du bist in verschiedenen Vereinen 
  und willst nicht überall ein anderes System nutzen?
- Du willst einen sicheren Messenger für deine Initiative, 
  der niemanden ausschließt?
- Was ist [matrix]?

freiburg.social und die StadtWandler laden ein zu einem Abend über 
die sichere Kommunikation mit dem matrix Protokoll.   
Geboten werden Hintergrundinformationen, Impulse und Diskussion darüber, 
wie Instant Messaging verbessert und bereichert werden kann. 
Im Anschluss besteht die Möglichkeit, gleich vor Ort ein Konto anzulegen 
und die Sache auszuprobieren.

Die Veranstaltung im Haus des Engagements richtet sich 
an Freiburger Initiativen, Vereine und andere Engagierte. 
Die Teilnahme ist kostenlos, Spenden gerne gesehen.

Eine Anmeldung ist nicht notwendig, hilft uns aber sehr bei der Vorbereitung.

freiburg.social ist ein 2021 eingetragener Verein mit dem Ziel, 
die Schaffung vertrauenswürdiger, 
datensparsamer und sicherer digitaler Dienste 
auf Basis freier Software zu fördern. 
Selbst betreibt freiburg.social derzeit eine 
Instanz des verteilten Mikroblogging-Dienstes Mastodon und einen 
Messenger-Dienst auf Grundlage des Matrix Kommunikationsprotokolls.

<a href="https://www.stadtwandler.org/de" target="blank">StadtWandler</a> 
macht Orte, Aktionen und Ideen für den sozial-ökologischen Wandel sichtbar. 
Du erfährst, was geschieht in Puncto Nachhaltigkeit in 
Initiativen, Firmen und Stadtverwaltung. 
StadtWandler zeigt, was jede:r Einzelne tun kann, 
um das Freiburg von morgen mitzugestalten und was es bringt. 
Abonniere die wöchentliche 
<a href="https://www.stadtwandler.org/aktionspost" target="blank">
StadtWandler-Aktionspost</a>

### Anmeldung
Da die Plätze für den Workshop begrenzt sind, bitten wir euch um kurze 
<a href="https://www.stadtwandler.org/de/events/lokal-verwurzelt-weltweit-vernetzt-lerne-einen-dezentralen-messenger-aus-freiburg-kennen" target="blank">
Anmeldung</a>.
