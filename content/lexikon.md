---
Title: "Lexikon"
description: "Begrifflichkeiten"
# date: "2019-02-06T00:00:00"
# image: 'images/logo.svg'
# tags: ["tag1","tag2"]
---

In unserer Satzung haben wir festgeschrieben, dass die Aufklärung und Bildung zu den Themen **freie Software**, **sichere digitale Kommunikation** und **Datensparsamkeit** zu unseren erklärten Zielen gehört. Dem möchten wir unter anderem mit diesem stetig wachsenden Lexikon gerecht werden.

Gibt es einen Begriff, der Euch hier fehlt? Immer her damit, wir versuchen ihn **verständlich** zu erklären!  
Ist ein Begriff dabei, dessen Erklärung Ihr nicht versteht? Gebt uns gerne Bescheid - wir möchten, dass ihn wirklich **alle** verstehen können.

### **F**

#### Fediverse

...

### **S**

#### Schattenprofile

...
