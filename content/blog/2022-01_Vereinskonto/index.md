+++
title = 'Unser Weg zum Vereinskonto'
slug = 'vereinskonto'
date = "2022-01-02"
description = 'Bald haben wir endlich unser lang ersehntes Vereinskonto. In diesem Artikel erzählen wir euch, warum das gar nicht so einfach war wie es klingt und wie wir doch noch fündig geworden sind.'
tags = ["Verein","Finanzen"]
+++

Seit Mitte 2021 sind wir nun ein "echter", also beim Registergericht eingetragener Verein. Wir haben eine Satzung, veranstalten regelmäßige Treffen und haben auch schon offizielle Mitgliederversammlungen ausgerichtet, bei denen wir zum Beispiel unseren Vorstand gewählt haben.

Auch betreiben wir ja schon länger einen [Matrix](/angebote/matrix)-Server und eine [Mastodon](/angebote/mastodon)-Instanz, haben also eine rege Vereinsaktivität. Die Server und Domains, die wir für diese Angebote mieten, laufen aber derzeit noch über verschiedene Mitglieder:innen. Das wollen wir so schnell wie möglich ändern, wozu uns aber eines bisher noch fehlte: Ein Vereinskonto. Also so eines mit Geld drinnen... bei einer Bank.

## Die Suche geht los

Wer sich in letzter Zeit mal privat mit der Suche nach einem Giro-Konto beschäftigt hat, hat vielleicht auch gemerkt, dass sich der Markt etwas gewandelt hat: Man bekommt längst nicht mehr bei jeder Bank ein Konto ohne Kontoführungsgebühren, Überweisungskosten und mit kostenlosen Karten. Wenn ich es richtig verstehe, liegt das an den niedrigen Zinsen, wodurch die Banken kaum noch Gewinne mit dem Geld ihrer Kund:innen erwirtschaften können.

Für einen Verein kommen Privat-Girokonten sowieso nicht in Frage, ob es auch verboten wäre ein Privatkonto einfach für den Verein zu nutzen, weiß ich nicht, aber es wäre auf jeden Fall sehr unpraktisch, da nur eine Person darüber verfügen könnte. Spezielle Vereinskonten gibt es quasi nirgends, es wird in der Regel auf die Angebote für Geschäftskunden verwiesen.

Anfragen bei Sparkasse und Volksbank, die sich gerne als Unterstützer lokaler Initiativen inszenieren, bleiben ernüchternd. Kontoführungsgebühren und vor allem Buchungskosten sind einfach zu hoch, besonders, da wir mit Unterstützung in Form von Kleinstbeträgen rechnen. Sollte uns zum Beispiel eine Nutzerin monatlich einen Euro überweisen, müssten wir der Sparkasse Freiburg davon 35 Cent abtreten. Die Volksbank Freiburg hat sogar die Vorraussetzung, dass alle Vorstandsmitglieder bereits ihre Privatkonten bei diesem Institut haben müssen. Die einzige Bank mit einem expliziten Angebot für ein kostenloses Vereinskonto war zum Zeitpunkt der Suche die (mir bis dahin nicht bekannte) Deutsche Skatbank.

## Nachhaltig soll es sein

Bei einem späteren Arbeitstreffen kamen wir zu dem Beschluss, dass wir unser Konto gerne bei einer nachhaltig wirtschaftenden Bank haben wollen. Was genau eine nachhaltige Bank ausmachen und welche Angebote es in dem Sektor gerade gibt, kann man ganz gut bei 
<a href="https://www.utopia.de" target="blank">Utopia.de</a> 
nachlesen, zum Beispiel in 
<a href="https://utopia.de/ratgeber/alternative-gruene-bank/" target="blank">diesem</a> 
Artikel. Wir schrieben also die uns bekannten nachhaltigen Banken mit Girokonto-Angeboten an.

Letzendlich hatten wir von der Ethikbank, der GLS-Bank und der Triodos-Bank Angebote vorliegen, die für uns in Frage kamen. Die besten Konditionen und auch den freundlichsten Kontakt hatten wir zur ursprünglich aus den Niederlanden stammenden Triodos Bank. Diese bietet ihr 
<a href="https://www.triodos.de/bezahlen/ggeschaeftskonto/" target="blank">gGeschäftskonto</a> 
entegen dem Namen auch für Vereine an, die nicht offiziell gemeinnützig sind, aber trotzdem zu den nachhaltigen Zielen und Werten der Bank passen. Dafür mussten wir einen Antrag schreiben und wurden auch nochmal in einem netten, persönlichen Telefongespräch abgeklopft.

Jetzt ist die Eröffnung des Kontos beauftragt und wir warten vorfreudig auf Post.

## Wie geht es weiter?

Sobald wir Zugriff auf das Konto haben, wollen wir alle Server und Domains von den Mitglieder:innen auf den Verein überschreiben und zentral über das Konto abrechnen. Ab diesem Zeitpunkt ist der Verein dann auch auf Spenden angewiesen. Wir haben vor, in regelmäßigen transparenten Finanzberichten genau darzulegen, wie unser derzeitger Bedarf aussieht und ob er gedeckt ist.
