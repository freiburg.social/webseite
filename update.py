import argparse
import subprocess
import sys


def get_git_head() -> str:
    """
    Get the commit for the current HEAD as a str.
    """
    git_head = subprocess.run(["git", "rev-parse", "HEAD"], capture_output=True)
    if git_head.returncode != 0:
        print(f"Error while getting git head: '{git_head.stderr.decode()}'")
        sys.exit(git_head.returncode)

    return git_head.stdout.decode()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Updates website from git branch")
    parser.add_argument(
        "--force", action="store_true", help="forces build even if not update in git"
    )
    args = parser.parse_args()

    previous_head = get_git_head()

    git_pull = subprocess.run(["git", "pull"], capture_output=True)
    if git_pull.returncode != 0:
        print(f"Error while pulling git repo: '{git_pull.stderr.decode()}'")
        sys.exit(git_pull.returncode)

    if previous_head == get_git_head() and not args.force:
        print("No update in git. Nothing to do.")
        sys.exit(0)

    hugo = subprocess.run(["hugo"], capture_output=True)

    if hugo.returncode != 0:
        print(f"Error while building website: '{hugo.stderr.decode()}'")

    print("Updated Website.")
