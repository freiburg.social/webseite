---
Title: "Datenschutzinformation Matrix"
description: "Wo steht die Beschreibung?"
## date: "2019-02-06T00:00:00"
## tags: ["tag1","tag2"]
## Size param refers to the css class for the <section> tag
## Opions are: xsmall, small, medium, large, xlarge, max
## Default is 'auto'
## size: "medium"
---

Datenschutzinformationen für den Matrix-Dienst von freiburg.social e. V. gemäß DSGVO  
*Zur Erläuterung: Unser Matrix-Dienst besteht aus dem "Homeserver" `https://matrix.freiburg.social`, sowie dem webbasierten Element-Messenger ([element.freiburg.social](https://element.freiburg.social/)).*


### Angaben zum Verantwortlichen

**freiburg.social e. V.**  
im Haus des Engagements  
Rehlingstraße 9 (Innenhof)  
79100 Freiburg im Breisgau

[verein@freiburg.social](mailto:verein@freiburg.social)

Weitere Angaben zum Verein und dem aktuellen Vorstand, siehe [Impressum](/impressum/).


### Datenschutzbeauftragter

Der Verein hat Hendrik vom Lehn zum Datenschutzbeauftragten gemäß Art. 37 DSGVO benannt. Er ist per E-Mail unter [datenschutz@freiburg.social](mailto:datenschutz@freiburg.social) oder unter der oben angegebenen postalischen Anschrift mit dem Zusatz Datenschutzbeauftragter erreichbar.


### Welcher Zweck wird mit der Datenverarbeitung verfolgt?

“Matrix” ist ein offener, dezentraler Kommunikationsdienst für die Echtzeitkommunikation. Es wird den Mitgliedern von freiburg.social e. V., sowie anderen Interessierten ermöglicht, mittels eines Matrix-Accounts mit anderen Nutzerinnen und Nutzern dieses Servers sowie weiteren Matrix-Nutzenden anderer Matrix-Server per Chat sowie Audio-/Video-Telefonie zu kommunizieren.


### Welche personenbezogenen Daten werden verarbeitet?

Die Verarbeitung umfasst folgende personenbezogene Daten:

*  Zugangsverwaltung: Matrix-ID, Anzeigename, E-Mail-Adresse (optional), Telefonnummer (optional)
*  Authentifizierung: Nutzername und Passwort
*  Benutzerinhalte: alle Daten, welche der Nutzer in das System eingibt (Ende-zu-Ende-verschlüsselt ist in 1:1-Räumen default)
*  Geräteidentifikation: IP-Adressen mit Zeitstempel und Gerätename; verwendete Art des Endgerätes (Mobil / Desktop), Betriebssystem
*  Serverprotokoll: IP-Adressen mit Zeitstempel
*  Audio-/Video-Telefonie: IP-Adressen, Audio-/Video-Daten
*  Benachrichtigungen (E-Mail)


### Wie lange werden die personenbezogenen Daten gespeichert?

Die personenbezogenen Daten werden spätestens nach 15 Monaten Inaktivität von unserem Server gelöscht. Die Löschaufträge werden an die föderierten Server weitergeleitet. Auf deren Ausührung haben wir jedoch keinen Einfluß.


### Wo werden die personenbezogenen Daten gespeichert?

Wir betreiben unseren Matrix-Dienst auf Servern der Firma [Netcup](https://www.netcup.de/).


### Betroffenenrechte

Wenn wir personenbezogene Daten von dir verarbeiten, hast du folgende Rechte:
* ein Recht auf Auskunft über die verarbeiteten Daten und auf Kopie,
* ein Berichtigungsrecht, wenn wir falsche Daten über dich verarbeiten,
* ein Recht auf Löschung, es sei denn, dass noch Ausnahmen greifen, warum wir die Daten noch speichern, also zum Beispiel Aufbewahrungspflichten oder Verjährungsfristen
* ein Recht auf Einschränkung der Verarbeitung,
* ein jederzeitiges Recht, Einwilligungen in die Datenverarbeitung zu widerrufen,
* ein Widerspruchsrecht gegen eine Verarbeitung im öffentlichen oder bei berechtigtem Interesse,
* ein Recht auf Datenübertragbarkeit,
* ein Beschwerderecht bei einer Datenschutz-Aufsichtsbehörde, wenn du der Ansicht bist, dass wir deine Daten nicht ordnungsgemäß verarbeiten. Für unseren Verein ist der Landesbeauftragte für den Datenschutz und die Informationsfreiheit in Baden-Württemberg zuständig. Wenn du dich in einem anderen Bundesland oder nicht in Deutschland aufhälst, kannst du dich aber auch an die dortige Datenschutzbehörde wenden.


### Quellenangaben/Lizenz

Das Grundgerüst dieser Datenschutzinformation haben wir mit Hilfe des [DS-GVO.clever-Tools](https://www.baden-wuerttemberg.datenschutz.de/ds-gvo.clever/) der Baden-Württembergischen Datenschutzaufsichtsbehörde erstellt und an unsere Bedürfnisse angepasst. Weiter haben wir Teile der [Datenschutzerklärung der TU-Dresden](https://doc.matrix.tu-dresden.de/privacy/) für uns angepasst übernommen. Text ist lizenziert unter [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de).
