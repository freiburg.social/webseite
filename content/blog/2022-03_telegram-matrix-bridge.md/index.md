+++
title = 'Telegram über Matrix verwenden: Die Telegram-Brücke'
slug = 'telegram-bridge'
date = "2022-03-17"
description = 'In diesem Artikel zeigen wir euch, wie ihr mit Telegram-User*innen direkt aus eurem Matrix-Client heraus schreiben könnt.'
tags = ["Matrix","Telegram","Messenger"]
+++

Wenn ihr diesen Artikel lest, stehen die Chancen ganz gut, dass ihr euch mit dem Thema Messenger schon so tief beschäftigt habt, dass ihr bereits Versuche hinter euch habt, Freund*innen dazu zu überreden, doch von Messenger A zu Messenger B zu wechseln. Das ist ein besonders großer Spaß bei Gruppen.

## Brücken im Allgemeinen

Eine Möglichkeit diesem Problem ein bisschen aus dem Weg zu gehen sind Brücken. Brücken sind Programme, die Nachrichten von einem Messenger-Protokoll ins andere übersetzen können, um so Kommunikation über die Grenzen der Verschiedenen Netzwerke hinaus zu ermöglichen. Für Matrix gibt es eine ganze Menge <a href="https://matrix.org/bridges/" target="blank">Brücken</a> für verschiedene Messenger, wobei das schon fast zu eng gefasst ist, gibt es doch auch eine <a href="" target="blank">E-Mail-Brücke</a> und eine <a href="https://matrix.org/bridges/#mastodon" target="blank">Mastodon-Brücke</a>.

Je nach Art und Beschaffenheit der zwei zu verbindenden Netzwerke kommen Brücken eigentlich immer mit Einschränkungen. Sie können zum Beispiel nur maximal die Schnittmenge der Features beider Messenger bedienen. Ein Beispiel: Im Signal Messenger kann man bereits geschickte Nachrichten nicht nochmals bearbeiten, somit unterstützt die Signal-Brücke das Nachrichten bearbeiten Feature von Matrix nicht.
Auch wird Ende-zu-Ende-Verschlüsselung eigentlich immer aufgebrochen, wenn die beiden Netzwerke nicht zufällig das gleiche Encryption-Protokoll verwenden. Im besten Fall gibt es dann eine Ende-zu-Brücke-Verschlüsselung, wo die Nachricht verschlüsselt an der Brücke ankommt, dort entschlüsselt und gleich wieder verschlüsselt wird.

## Die Telegram-Brücke

Kurzer Exkurs: Der Messenger Telegram hat in der Vergangenheit viel schlechte Presse bekommen, weil er zunehmend zum Sammelbecken von Rechtsextremisten und Anhänger*innen von Verschwörungsmythen geworden ist. Außerdem ist er von den "handelsüblichen"-Messengern der mit der schlechtesten Datensicherheit: Direktnachrichten sind nur dann Ende-zu-Ende-verschlüsselt, wenn man das explizit anschaltet und bei Gruppen gibt es das Feature überhaupt nicht.

Wenn ihr aber noch ein paar Kontakte oder Gruppen auf Telegram habt und euch nicht die Mühe machen wollt, die alle in die Matrix zu holen, zeigen wir euch jetzt, wie ihr Telegram von euren Endgeräten löschen könnt und trotzdem mit allen in Kontakt bleiben könnt.

Und noch ein kurzer Exkurs: Die Telegram-Brücke unterstützt zwei Arten von Brücken: Puppeting und Relay. In diesem Artikel schauen wir uns nur Puppeting an, bei dem die Brücke eure Aktionen im Matrix Netzwerk direkt durch euren Telegram-Account im Telegram Netzwerk nachspielt, wie mit einer Handpuppe. Ebenso wird für jeden Telegram-User mit dem ihr interagiert ein extra Matrix-User angelegt und ebenfalls wie eine Handpuppe von der Brücke gesteuert. Eure Telegram-Kontakte bekommen also gar nicht mit, dass ihr die Brücke benutzt. Nachteil: Ihr könnt so keine Räume erstellen in denen Matrix-User und Telegram-User sind. Das geht dann im Relay-Modus.

Jetzt aber weiter im Text. Um den Puppeting-Modus zu aktivieren braucht ihr einen Telegram-Account und einen [Matrix-Account auf freiburg.social](/angebote/matrix/).

### 1) Im Telegram-Netzwerk anmelden

Die gesamte Bedienung der Telegram-Brücke läuft über einen speziellen Bot, den Telegram bridge bot. Startet in eurem Matrix-Client einen Unterhaltung mit ihm unter seinem Usernamen `@telegrambot:freiburg.social`.

Wenn ihr ihm die Nachricht `help` schreibt, bekommt ihr ziemlich ausführliche Informationen zu allem, was der Bot so kann.

⚠️⚠️⚠️ ACHTUNG ⚠️⚠️⚠️: Spätestens jetzt müsst ihr euch sicher sein, der Brücke und den Betreiber*innen (also uns) hinreichend zu vertrauen, denn ihr gebt der Brücke vollen Zugriff auf euren Telegram-Account.

Um euch an eurem Telegram-Account anzumelden, schickt die Nachricht `login` und folgt den Anweisungen. Wenn alles geklappt hat, seid ihr danach angemeldet und solltet in eurer Matrix-Raumliste alle Gruppen sehen, in denen ihr in Telegram Mitglied seid. Ihr könnt dort direkt anfangen zu schreiben, allerdings wird die History von vor eurem Login nicht sichtbar sein.

### 2) Direktnachrichten

Sobald euch jemand in Telegram anschreibt, bekommt ihr in Matrix eine Einladung zu einem Direktnachrichtenraum und könnt dort dann wie gewohnt hin und her schreiben.

Wenn ihr selber eine Direktnachricht mit einem Telegram-Nutzer initiieren wollt, mit dem ihr noch nicht über die Brücke geschrieben habt, könnt ihr diesen mit Hilfe des Telegram bridge bots suchen. Schreibt dem Bot eine Nachricht nach diesem Muster: `search <Suchbegriff>` und benutzt als Suchbegriff den Kontaktnamen in Telegram. Der Bot sollte euch eine Liste von passenden Telegram-Accounts bzw deren Matrix-Puppets anzeigen, die ihr dann anschreiben könnt.


## Weitere Infos

Das wars auch schon. Wenn ihr mehr Informationen zur Telegram-Brücke sucht, könnt ihr euch die entsprechende Seite auf <a href="https://matrix.org/bridges/#telegram" target="blank">Matrix.org</a> ansehen oder direkt den Quellcode auf <a href="https://github.com/mautrix/telegram" target="blank">Github</a> studieren.
