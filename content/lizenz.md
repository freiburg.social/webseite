---
Title: "Lizenzen"
description: "Lizenzen und Quelldateien von unseren Erzeugnissen"
# date: "2021-06-11T00:00:00"
image: 'images/logo-lizenz.png'
# tags: ["tag1","tag2"]
---

## Logo

Unser Logo steht unter der Lizenz <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de" target="_blank">CC BY-SA 4.0</a>.

{{< image src="/logo/freiburgsocial_Logo_V1_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.png" class="small" >}}
{{< image src="/logo/freiburgsocial_Logo_V2_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.png" class="small" >}}
{{< image src="/logo/freiburgsocial_Logo_V3_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.png" class="small" >}}

Urheber:innen sind:
- Karin Seyfarth und Olav Seyfarth als Urheber:innen des Ufos
- [Imke Senst](https://www.imke-senst.de/) als Urheberin des Logos

Download:
- <a href="/logo/freiburgsocial_Logo_V1_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.svg" target="_blank">V1 SVG</a>
- <a href="/logo/freiburgsocial_Logo_V1_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">V1 PDF</a>
- <a href="/logo/freiburgsocial_Logo_V2_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.svg" target="_blank">V2 SVG</a>
- <a href="/logo/freiburgsocial_Logo_V2_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">V2 PDF</a>
- <a href="/logo/freiburgsocial_Logo_V3_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.svg" target="_blank">V3 SVG</a>
- <a href="/logo/freiburgsocial_Logo_V3_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">V3 PDF</a>

## Postkarte «Postkarte war gestern»

Unsere Postkarte «Postkarte war gestern» steht unter der Lizenz <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de" target="_blank">CC BY-SA 4.0</a>. Wir haben sie bei <a href="https://www.citydruck-freiburg.de/" target="_blank">Citydruck Freiburg</a> drucken lassen.

{{< image src="/postkarte/freiburgsocial_Postkarte-war-gestern_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst-1.png" class="fit" >}}
{{< image src="/postkarte/freiburgsocial_Postkarte-war-gestern_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst-2.png" class="fit" >}}

Urheber:innen sind:
- Karin Seyfarth und Olav Seyfarth als Urheber:innen des Ufos
- <a href="https://www.imke-senst.de" target="_blank">Imke Senst</a> als Urheberin des Logos und des Flyers

Download:
- <a href="/postkarte/freiburgsocial_Postkarte-war-gestern_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">PDF</a>
- <a href="/postkarte/freiburgsocial_Postkarte-war-gestern_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.tar.gz" target="_blank">Scribus-Datei als .tag.gz</a>
- <a href="/fonts/Open_Sans.tar.gz" target="_blank">OpenSans: Verwendeter Font für Fließtexte (Apache License Version 2.0)</a>
- <a href="/fonts/Soapy_Hands_v1.2.tar.gz" target="_blank">Soapy Hands v1.2: Verwendeter Font für Sprechblasen (SIL OPEN FONT LICENSE Version 1.1)</a>
- Verwendetes Farbprofil ISO Coated v2 (ECI) erhält man bei der <a href="http://www.eci.org/de/downloads" target="_blank">ECI</a>

## Roll-Up 2022

Unser Roll-Up steht unter der Lizenz <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de" target="_blank">CC BY-SA 4.0</a>. Wir haben es beim <a href="https://www.umweltdruckhaus.de/" target="_blank">UmweltDruckhaus Hannover</a> produzieren lassen.

{{< image src="/rollup/freiburgsocial_foto-rollup_CC-BY-SA_ImkeSenst.png" class="small" >}}

Urheber:innen sind:
- Karin Seyfarth und Olav Seyfarth als Urheber:innen des Ufos
- <a href="https://www.imke-senst.de" target="_blank">Imke Senst</a> als Urheberin des Logos und des Roll-Ups

Download:
- <a href="/rollup/freiburgsocial_rollup_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">PDF</a>
- <a href="/rollup/freiburgsocial_rollup_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst-Seite001.tar.gz" target="_blank">Scribus-Datei als .tag.gz</a>
- <a href="/fonts/Open_Sans.tar.gz" target="_blank">OpenSans: Verwendeter Font für Fließtexte (Apache License Version 2.0)</a>
- <a href="/fonts/Soapy_Hands_v1.2.tar.gz" target="_blank">Soapy Hands v1.2: Verwendeter Font für Sprechblasen (SIL OPEN FONT LICENSE Version 1.1)</a>
- Verwendetes Farbprofil ISO Coated v2 (ECI) erhält man bei der <a href="http://www.eci.org/de/downloads" target="_blank">ECI</a>

## Sticker

Unser Sticker steht unter der Lizenz <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de" target="_blank">CC BY-SA 4.0</a>. Wir haben ihn als veganen Aufkleber bei <a href="https://deinestadtklebt.de/" target="_blank">DeineStadtKlebt.de</a> produzieren lassen.

{{< image src="/sticker/Sticker_in_Karton.jpg" class="small" >}}

Urheber:innen sind:

* Karin Seyfarth und Olav Seyfarth als Urheber:innen des Ufos
* <a href="https://www.imke-senst.de" target="_blank">Imke Senst</a> als Urheberin des Logos und Stickers

Download:
- <a href="/sticker/freiburgsocial_sticker_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.pdf" target="_blank">PDF</a>
- <a href="/sticker/freiburgsocial_sticker_CC-BY-SA_KarinSeyfarth-OlavSeyfarth-ImkeSenst.tar.gz" target="_blank">Scribus-Datei als .tag.gz</a>
- <a href="/fonts/Soapy_Hands_v1.2.tar.gz" target="_blank">Soapy Hands v1.2: Verwendeter Font für Sprechblasen (SIL OPEN FONT LICENSE Version 1.1)</a>
- Verwendetes Farbprofil ISO Coated v2 (ECI) erhält man bei der <a href="http://www.eci.org/de/downloads" target="_blank">ECI</a>


## Flyer 2022

Unser Flyer steht unter der Lizenz <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de" target="_blank">CC BY-SA 4.0</a>.

{{< image src="/flyer/freiburgsocial_Infoflyer2022_CC-BY-SA_ImkeSenst_Seite1.png" class="fit" >}}
{{< image src="/flyer/freiburgsocial_Infoflyer2022_CC-BY-SA_ImkeSenst_Seite2.png" class="fit" >}}

Urheber:innen sind:
- Karin Seyfarth und Olav Seyfarth als Urheber:innen des Ufos
- <a href="https://www.imke-senst.de" target="_blank">Imke Senst</a> als Urheberin des Logos und des Flyers

Download:
- <a href="/flyer/freiburgsocial_Infoflyer2022_CC-BY-SA_ImkeSenst.pdf" target="_blank">PDF</a>
- <a href="/flyer/freiburgsocial_Infoflyer2022_CC-BY-SA_ImkeSenst.tar.gz" target="_blank">Scribus-Datei als .tag.gz</a>
- <a href="/fonts/Open_Sans.tar.gz" target="_blank">OpenSans: Verwendeter Font für Fließtexte (Apache License Version 2.0)</a>
- <a href="/fonts/Soapy_Hands_v1.2.tar.gz" target="_blank">Soapy Hands v1.2: Verwendeter Font für Sprechblasen (SIL OPEN FONT LICENSE Version 1.1)</a>
- Verwendetes Farbprofil ISO Coated v2 (ECI) erhält man bei der <a href="http://www.eci.org/de/downloads" target="_blank">ECI</a>
